import express, { Request, Response, Express, NextFunction } from "express";
import axios from "axios";

interface IPersonResult {
  name: string;
  height: string;
  mass: string;
  hair_color: string;
  skin_color: string;
  eye_color: string;
  birth_year: string;
  gender: string;
  homeworld: string;
  films: string[];
  species: string[];
  vehicles: any[];
  starships: any[];
  created: string;
  edited: string;
  url: string;
}

interface IPlanetResult {
  name: string;
  rotation_period: string;
  orbital_period: string;
  diameter: string;
  climate: string;
  gravity: string;
  terrain: string;
  surface_water: string;
  population: string;
  residents: string[];
  films: string[];
  created: string;
  edited: string;
  url: string;
}

const app: Express = express();
const port = process.env.PORT || 4000;

app.use(express.json());

app.get("/", (_req: Request, res: Response, _next: NextFunction) => {
  res.status(200).send("Application started!");
});

// Normally I would seperate these endpoints into route and controller files, but I decided to save the unwanted complexity here.

app.get("/people", async (req: Request, res: Response, _next: NextFunction) => {
  // Function flow:
  // - We make the inital request to our default /people endpoint
  // - We save the response of the request into a variable to be reused in our while loop
  // - While there is a .next attribute of our response data we will continure looping
  // - We then use a switch block to determine if we send back the unmutated response, or if we need to sort it

  let results: IPersonResult[] = [];
  let request = await axios
    .get(`https://swapi.dev/api/people`)
    .then((res) => {
      results.push(...res.data.results);
      return res.data;
    })
    .catch(() => res.status(500).send("Error"));

  while (request.next !== null) {
    request = await axios.get(request.next).then((res) => {
      results.push(...res.data.results);
      return res.data;
    });
  }

  const sortByName = (a: IPersonResult, b: IPersonResult) => {
    const nameA = a.name.toUpperCase();
    const nameB = b.name.toUpperCase();

    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }

    return 0;
  };

  const sortByHeight = (a: IPersonResult, b: IPersonResult) => {
    return +a.height > +b.height ? 1 : -1;
  };

  const sortByMass = (a: IPersonResult, b: IPersonResult) => {
    return +a.mass > +b.mass ? 1 : -1;
  };

  switch (req.query.sortBy) {
    case "name":
      res.status(200).send(results.sort(sortByName));
      break;
    case "height":
      res.status(200).send(results.sort(sortByHeight));
      break;
    case "mass":
      res.status(200).send(results.sort(sortByMass));
      break;

    default:
      res.status(200).send(results);
      break;
  }
});

app.get(
  "/planets",
  async (_req: Request, res: Response, _next: NextFunction) => {
    // Function flow:
    // - This endpoint works almost exactly the same as our /people endpoint, however here we use a bit of extra logic to loop through the residents field and
    // replace the provided URL with the name of the character returned from said URL

    let results: IPlanetResult[] = [];

    let request = await axios
      .get(`https://swapi.dev/api/planets`)
      .then((res) => {
        return res.data;
      });

    while (request.next !== null) {
      await axios.get(request.next).then(async (res) => {
        await res.data.results.forEach((planet: IPlanetResult) => {
          planet.residents.forEach(async (residentUrl: string, idx: number) => {
            const name = await axios
              .get(residentUrl)
              .then((res) => res.data.name);
            planet.residents[idx] = name;
          });
        });

        results.push(...res.data.results);

        request = res.data;
      });
    }

    // This timeout is just used as a buffer to make sure the last few resident promises arent lagging behind
    setTimeout(() => {
      res.status(200).send([...results]);
    }, 2000);
  }
);

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
